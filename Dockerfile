FROM node
EXPOSE 31330
WORKDIR /code
CMD [ "npm", "start" ]
RUN mkdir -p /code
COPY env.ini slacked app.js package.json /code/
RUN npm install