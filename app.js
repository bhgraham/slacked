// Benjamin H. Graham <bman@bman.io>
// slackedapi
// simple api to wrap around the slackpost script to send messages to slack
// http://[::]:31330/chan/nick/icon/color/msg
var restify = require('restify'),
	child_process = require('child_process'),
	server = restify.createServer({
	name: 'slackedapi',
	version: '0.0.1'
});
process.title = 'SlackedAPI';
server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.get('/:chan/:nick/:icon/:color/:msg', function (req, res, next) {
	var chan = req.params.chan,
		msg = req.params.msg,
		nick = req.params.nick,
		icon = req.params.icon,
		color = req.params.color,
		statusCode = 200;
	switch (req.params.chan) {
		case 'devops':
			console.log('Sending as %s, with icon %s and color %s to channel %s, message:%s', nick, icon, color, chan, msg);
			child_process.execFile('/bin/bash', ['./slacked', chan, nick, icon, color, msg], function(error, stdout, stderr){
				console.log(stdout);
			});
			break;
		default:
			console.log('Sending to %s is not allowed.', req.params.chan);
			statusCode = 400;
			break;
	}
	res.send(statusCode);
	return next();
});
server.listen(31330, function () {
	console.log('%s listening at %s', server.name, server.url);
});